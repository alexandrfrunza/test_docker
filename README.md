### Ruby On Rails implementation of RealWorld app

This Rails app is part of the [RealWorld](https://github.com/gothinkster/realworld) project.

You might also check [Laravel version](https://github.com/alexeymezenin/laravel-realworld-example-app) of this app.

See how the exact same Medium.com clone (called [Conduit](https://demo.realworld.io)) is built using different [frontends](https://codebase.show/projects/realworld?category=frontend) and [backends](https://codebase.show/projects/realworld?category=backend). Yes, you can mix and match them, because **they all adhere to the same [API spec](https://gothinkster.github.io/realworld/docs/specs/backend-specs/introduction)**

### How to run the API in docker

Open in terminal your app direction.

Run:
```
$docker-compose up
```
Execute migrations:
```
$docker exec <container id> rake db:migrate
```
You can find your running project on:
```
http://127.0.0.1:4040/api/articles
```
### Switching to SQLite Database

1) Open the docker-compose.yml file in the project root directory.
2) Comment out or remove the entire db service section, including the image, networks, and environment fields. It should look like this:

# db:
#   image: postgres:13-alpine
#   networks:
#     static_network:
#       ipv4_address: 172.16.0.3
#   environment:
#     - POSTGRES_USER=your_username
#     - POSTGRES_PASSWORD=your_password
#     - POSTGRES_DB=your_database_name

3) Save the changes to the docker-compose.yml file.
4) Open the database.yml file in the project root directory.
5) Modify the development and test sections as follows:

development:
  <<: *default
  adapter: sqlite3
  database: db/development.sqlite3

test:
  <<: *default
  adapter: sqlite3
  database: db/test.sqlite3

6) Save the changes to the database.yml file.
7) Change in you Gemfile gem 'pg' to:
gem 'sqlite3', '~> 1.3.0'
8) run:
```
$docker-compose up
```

### Switching to Postgres Database

1) Uncomment or add back the db service section in the docker-compose.yml file. It should look like this:

db:
  image: postgres:13-alpine
  networks:
    static_network:
      ipv4_address: 172.16.0.3
  environment:
    - POSTGRES_USER=your_username
    - POSTGRES_PASSWORD=your_password
    - POSTGRES_DB=your_database_name

2) Save the changes to the docker-compose.yml file.

3) Open the database.yml file in the project root directory.

4) Modify the development and test sections back to their original state:
development:

  <<: *default
  adapter: postgresql
  encoding: unicode
  database: your_database_name
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  username: your_username
  password: your_password
  host: db

test:
  <<: *default
  adapter: postgresql
  encoding: unicode
  database: your_database_name
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  username: your_username
  password: your_password
  host: db

  5) Save the changes to the database.yml file.
  6) Change in you Gemfile gem 'sqlite3', '~> 1.3.0' to:
  gem 'pg'
  7) run:
```
$docker-compose up
```

### Migrations

1) docker exec <container_id> bundle exec rake db:create
2) docker exec <container_id> bundle exec rake db:migrate
3) docker exec <container_id> bundle exec rake db:seed (only development mode)